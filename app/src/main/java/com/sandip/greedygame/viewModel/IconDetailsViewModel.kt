package com.sandip.greedygame.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sandip.greedygame.modal.allIcon.IconModal
import com.sandip.greedygame.modal.iconDetails.IconDetailsModal
import com.sandip.greedygame.repository.IconDetailsRepository
import kotlinx.coroutines.launch

class IconDetailsViewModel: ViewModel() {

    private var iconDetailsRepository: IconDetailsRepository
    var iconDetailsLiveData: MutableLiveData<IconDetailsModal> = MutableLiveData()
    var allIconLiveData: MutableLiveData<IconModal> = MutableLiveData()

    init {
        iconDetailsRepository = IconDetailsRepository()
    }

    fun getIconDetails(iconSetId: Int){
        viewModelScope.launch {
            iconDetailsLiveData.value = iconDetailsRepository.getIconDetails(iconSetId)
        }
    }

    fun getAllIcon(iconSetId: Int){
        viewModelScope.launch {
            allIconLiveData.value = iconDetailsRepository.getAllIcon(iconSetId)
        }
    }
}