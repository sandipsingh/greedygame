package com.sandip.greedygame.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sandip.greedygame.modal.publicIconSets.Iconset
import com.sandip.greedygame.modal.searchIcons.Icon
import com.sandip.greedygame.repository.MainRepository
import kotlinx.coroutines.launch

class MainActivityViewModel: ViewModel() {

    private var mainRepository: MainRepository
    var iconSetsLiveData: MutableLiveData<List<Iconset>> = MutableLiveData()
    var searchIconLiveData: MutableLiveData<List<Icon>> = MutableLiveData()

    init {
        mainRepository = MainRepository()
    }

    fun getAllPublicIconSets() {
        viewModelScope.launch {
            iconSetsLiveData.value = mainRepository.getAllPublicIconSets()
        }
    }

    fun getSearchIcons(query: String) {
        viewModelScope.launch {
            searchIconLiveData.value = mainRepository.getSearchIcons(query)
        }
    }
}