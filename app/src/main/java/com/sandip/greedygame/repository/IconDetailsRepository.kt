package com.sandip.greedygame.repository

import android.util.Log
import com.sandip.greedygame.api.ApiService
import com.sandip.greedygame.api.RetrofitInstance
import com.sandip.greedygame.modal.allIcon.IconModal
import com.sandip.greedygame.modal.iconDetails.IconDetailsModal
import java.lang.Exception

class IconDetailsRepository {

    private var apiService: ApiService
    private lateinit var iconDetails: IconDetailsModal
    private lateinit var allIcon: IconModal

    init {
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService::class.java)
    }

    suspend fun getIconDetails(iconSetId: Int): IconDetailsModal {
        try {
            val response = apiService.getIconDetails(iconSetId)
            val body = response.body()
            if (body != null) {
                val data = body
                iconDetails = data
            }
        } catch (exception: Exception) {
            Log.i("IconDetailsRespository", "Exception: " + exception.message.toString())
        }

        return iconDetails
    }

    suspend fun getAllIcon(iconSetId: Int): IconModal {
        try {
            val response = apiService.getAllIcon(iconSetId)
            val body = response.body()
            if (body != null) {
                val data = body
                allIcon = data
            }
        } catch (exception: Exception) {
            Log.i("IconDetailsRespository", "Exception: " + exception.message.toString())
        }

        return allIcon
    }
}