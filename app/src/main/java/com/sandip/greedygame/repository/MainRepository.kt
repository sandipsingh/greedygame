package com.sandip.greedygame.repository

import android.util.Log
import com.sandip.greedygame.api.ApiService
import com.sandip.greedygame.api.RetrofitInstance
import com.sandip.greedygame.modal.publicIconSets.Iconset
import com.sandip.greedygame.modal.searchIcons.Icon
import java.lang.Exception

class MainRepository {

    private var apiService: ApiService
    private var iconSetList: List<Iconset> = ArrayList()
    private var searchIconList: List<Icon> = ArrayList()

    init {
        apiService = RetrofitInstance.getRetrofitInstance().create(ApiService::class.java)
    }

    suspend fun getAllPublicIconSets(): List<Iconset> {
        try {
            val response = apiService.getAllPublicIconSets(20)
            val body = response.body()
            if (body != null) {
                val data = body
                iconSetList = data.iconsets
            }
        } catch (exception: Exception) {
            Log.i("MainRespository", "Exception: " + exception.message.toString())
        }

        return iconSetList
    }

    suspend fun getSearchIcons(query: String): List<Icon> {
        try {
            val response = apiService.getSearchIcons(query, 20)
            val body = response.body()
            if (body != null) {
                val data = body
                searchIconList = data.icons
            }
        } catch (exception: Exception) {
            Log.i("MainRespository", "Exception: " + exception.message.toString())
        }

        return searchIconList
    }
}