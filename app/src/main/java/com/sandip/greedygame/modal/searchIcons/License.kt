package com.sandip.greedygame.modal.searchIcons

data class License(
    val license_id: Int,
    val name: String,
    val scope: String,
    val url: String
)