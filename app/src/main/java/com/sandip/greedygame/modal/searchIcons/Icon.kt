package com.sandip.greedygame.modal.searchIcons

data class Icon(
    val categories: List<Category>? = null,
    val containers: List<Container>? = null,
    val icon_id: Int,
    val is_icon_glyph: Boolean,
    val is_premium: Boolean,
    val prices: List<Price>? = null,
    val published_at: String,
    val raster_sizes: List<RasterSize>? = null,
    val styles: List<Style>,
    val tags: List<String>? = null,
    val type: String = "",
    val vector_sizes: List<VectorSize>
)