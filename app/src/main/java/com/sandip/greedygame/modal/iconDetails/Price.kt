package com.sandip.greedygame.modal.iconDetails

data class Price(
    val currency: String,
    val license: LicenseX,
    val price: Int
)