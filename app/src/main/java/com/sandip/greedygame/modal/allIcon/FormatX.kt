package com.sandip.greedygame.modal.allIcon

data class FormatX(
    val download_url: String,
    val format: String
)