package com.sandip.greedygame.modal.searchIcons

data class Container(
    val download_url: String,
    val format: String
)