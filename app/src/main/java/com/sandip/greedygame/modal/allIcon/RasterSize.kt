package com.sandip.greedygame.modal.allIcon

data class RasterSize(
    val formats: List<Format>,
    val size: Int,
    val size_height: Int,
    val size_width: Int
)