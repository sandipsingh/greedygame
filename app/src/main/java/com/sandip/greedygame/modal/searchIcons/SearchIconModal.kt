package com.sandip.greedygame.modal.searchIcons

data class SearchIconModal(
    val icons: List<Icon>,
    val total_count: Int
)