package com.sandip.greedygame.modal.searchIcons

data class Category(
    val identifier: String,
    val name: String
)