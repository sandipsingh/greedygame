package com.sandip.greedygame.modal.searchIcons

data class Price(
    val currency: String,
    val license: License,
    val price: Int
)