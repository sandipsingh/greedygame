package com.sandip.greedygame.modal.searchIcons

data class Style(
    val identifier: String,
    val name: String
)