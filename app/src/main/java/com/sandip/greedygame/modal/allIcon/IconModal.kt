package com.sandip.greedygame.modal.allIcon

data class IconModal(
    val icons: List<Icon>,
    val total_count: Int
)