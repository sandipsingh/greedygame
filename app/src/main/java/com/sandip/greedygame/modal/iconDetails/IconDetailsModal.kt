package com.sandip.greedygame.modal.iconDetails

data class IconDetailsModal(
    val are_all_icons_glyph: Boolean,
    val author: Author,
    val categories: List<Category>,
    val icons_count: Int,
    val iconset_id: Int,
    val identifier: String,
    val is_premium: Boolean,
    val license: License,
    val name: String,
    val prices: List<Price>,
    val published_at: String,
    val readme: String,
    val styles: List<Style>,
    val type: String,
    val website_url: String
)