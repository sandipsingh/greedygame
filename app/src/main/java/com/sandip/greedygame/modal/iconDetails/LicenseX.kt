package com.sandip.greedygame.modal.iconDetails

data class LicenseX(
    val license_id: Int,
    val name: String,
    val scope: String,
    val url: String
)