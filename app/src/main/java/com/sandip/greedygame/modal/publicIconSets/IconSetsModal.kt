package com.sandip.greedygame.modal.publicIconSets

data class IconSetsModal(
    val iconsets: List<Iconset>,
    val total_count: Int
)