package com.sandip.greedygame.modal.searchIcons

data class FormatX(
    val download_url: String,
    val format: String
)