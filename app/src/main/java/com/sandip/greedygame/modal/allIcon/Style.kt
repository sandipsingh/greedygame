package com.sandip.greedygame.modal.allIcon

data class Style(
    val identifier: String,
    val name: String
)