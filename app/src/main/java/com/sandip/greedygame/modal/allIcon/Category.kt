package com.sandip.greedygame.modal.allIcon

data class Category(
    val identifier: String,
    val name: String
)