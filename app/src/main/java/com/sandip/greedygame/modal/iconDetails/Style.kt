package com.sandip.greedygame.modal.iconDetails

data class Style(
    val identifier: String,
    val name: String
)