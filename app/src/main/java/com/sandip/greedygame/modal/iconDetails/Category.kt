package com.sandip.greedygame.modal.iconDetails

data class Category(
    val identifier: String,
    val name: String
)