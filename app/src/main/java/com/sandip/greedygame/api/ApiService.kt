package com.sandip.greedygame.api

import com.sandip.greedygame.modal.allIcon.IconModal
import com.sandip.greedygame.modal.iconDetails.IconDetailsModal
import com.sandip.greedygame.modal.publicIconSets.IconSetsModal
import com.sandip.greedygame.modal.searchIcons.SearchIconModal
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @Headers("Authorization: Bearer iOZ4Zp46c5rqYz9Zz3JFSyDAHJSmjlpbmuvo6TVH6bTCFiZt4FBEjwJTdHA9nKdl")
    @GET("v4/iconsets")
    suspend fun getAllPublicIconSets(@Query("count") count: Int): Response<IconSetsModal>

    @Headers("Authorization: Bearer iOZ4Zp46c5rqYz9Zz3JFSyDAHJSmjlpbmuvo6TVH6bTCFiZt4FBEjwJTdHA9nKdl")
    @GET("v4/icons/search")
    suspend fun getSearchIcons(@Query("query") query: String, @Query("count") count: Int): Response<SearchIconModal>

    @Headers("Authorization: Bearer iOZ4Zp46c5rqYz9Zz3JFSyDAHJSmjlpbmuvo6TVH6bTCFiZt4FBEjwJTdHA9nKdl")
    @GET("v4/iconsets/{iconset_id}")
    suspend fun getIconDetails(@Path("iconset_id") iconset_id: Int): Response<IconDetailsModal>

    @Headers("Authorization: Bearer iOZ4Zp46c5rqYz9Zz3JFSyDAHJSmjlpbmuvo6TVH6bTCFiZt4FBEjwJTdHA9nKdl")
    @GET("v4/iconsets/{iconset_id}/icons")
    suspend fun getAllIcon(@Path("iconset_id") iconset_id: Int): Response<IconModal>

}