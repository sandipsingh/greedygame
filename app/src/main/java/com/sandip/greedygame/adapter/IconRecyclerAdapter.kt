package com.sandip.greedygame.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sandip.greedygame.R
import com.sandip.greedygame.databinding.SearchListItemBinding
import com.sandip.greedygame.modal.allIcon.Icon
import com.sandip.greedygame.utils.GreedyGameConstants
import java.util.ArrayList

class IconRecyclerAdapter : RecyclerView.Adapter<IconRecyclerAdapter.IconViewHolder>() {

    private var iconList = ArrayList<Icon>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IconViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: SearchListItemBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.search_list_item, parent, false)
        return IconViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return iconList.size
    }

    override fun onBindViewHolder(holder: IconViewHolder, position: Int) {
        holder.bind(iconList.get(position))
    }

    fun setIconList(icon: List<Icon>) {
        iconList.addAll(icon)
        notifyDataSetChanged()
    }

    class IconViewHolder(val binding: SearchListItemBinding) :
            RecyclerView.ViewHolder(binding.root) {
        fun bind(icon: Icon) {
            Glide.with(binding.ivIcon)
                    .load(icon.raster_sizes!!.get(5).formats.get(0).preview_url)
                    .centerCrop()
                    .placeholder(R.drawable.no_img_avail)
                    .into(binding.ivIcon)
            binding.tvName.text = GreedyGameConstants.NA
            binding.tvPrice.text = GreedyGameConstants.NA
            binding.tvType.text = icon.type
            binding.tvAuthorName.text = GreedyGameConstants.NA
            binding.tvLicenceName.text = GreedyGameConstants.NA
        }
    }
}