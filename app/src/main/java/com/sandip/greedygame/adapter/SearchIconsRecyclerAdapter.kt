package com.sandip.greedygame.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sandip.greedygame.R
import com.sandip.greedygame.databinding.SearchListItemBinding
import com.sandip.greedygame.modal.searchIcons.Icon
import com.sandip.greedygame.utils.GreedyGameConstants
import java.util.ArrayList

class SearchIconsRecyclerAdapter(private val clickListener:(Icon)->Unit) :
    RecyclerView.Adapter<SearchIconsRecyclerAdapter.SearchIconViewHolder>() {

    private var iconSetList = ArrayList<Icon>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchIconViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: SearchListItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.search_list_item, parent, false)
        return SearchIconViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return iconSetList.size
    }

    override fun onBindViewHolder(holder: SearchIconViewHolder, position: Int) {
        holder.bind(iconSetList.get(position), clickListener)
    }

    fun setSearchList(list: List<Icon>) {
        iconSetList.addAll(list)
        notifyDataSetChanged()
    }

    class SearchIconViewHolder(val binding: SearchListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(searchIcon: Icon, clickListener:(Icon)->Unit) {
            Glide.with(binding.ivIcon)
                .load(searchIcon.raster_sizes!!.get(5).formats.get(0).preview_url)
                .centerCrop()
                .placeholder(R.drawable.no_img_avail)
                .into(binding.ivIcon)
            binding.tvName.text = GreedyGameConstants.NA
            binding.tvType.text = searchIcon.type
            if (searchIcon.prices != null) {
                binding.tvPrice.text =
                    searchIcon.prices!!.get(0).currency + " " + searchIcon.prices.get(0).price
                binding.tvLicenceName.text = searchIcon.prices!!.get(0).license.name
            } else {
                binding.tvPrice.text = GreedyGameConstants.NA
                binding.tvLicenceName.text = GreedyGameConstants.NA
            }
            binding.tvAuthorName.text = GreedyGameConstants.NA
            binding.rlIcons.setOnClickListener {
                clickListener(searchIcon)
            }
        }
    }
}