package com.sandip.greedygame.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sandip.greedygame.R
import com.sandip.greedygame.databinding.IconSetsListItemBinding
import com.sandip.greedygame.modal.publicIconSets.Iconset
import java.util.ArrayList

class IconSetsRecyclerAdapter(private val clickListener:(Iconset)->Unit) : RecyclerView.Adapter<IconSetsRecyclerAdapter.IconSetViewHolder>() {

    private var iconSetList = ArrayList<Iconset>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IconSetViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: IconSetsListItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.icon_sets_list_item, parent, false)
        return IconSetViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return iconSetList.size
    }

    override fun onBindViewHolder(holder: IconSetViewHolder, position: Int) {
        holder.bind(iconSetList.get(position), clickListener)
    }

    fun setIconSetList(iconList: List<Iconset>) {
        iconSetList.addAll(iconList)
        notifyDataSetChanged()
    }

    class IconSetViewHolder(val binding: IconSetsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(iconSet: Iconset, clickListener:(Iconset)->Unit) {
            binding.tvName.text = iconSet.name
            binding.tvPrice.text = iconSet.prices.get(0).currency+" "+iconSet.prices.get(0).price
            binding.tvType.text = iconSet.type
            binding.tvAuthorName.text = iconSet.author.name
            binding.tvLicenceName.text = iconSet.prices.get(0).license.name
            binding.rlIconSet.setOnClickListener {
                clickListener(iconSet)
            }
        }
    }
}