package com.sandip.greedygame.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.sandip.greedygame.R
import com.sandip.greedygame.adapter.IconRecyclerAdapter
import com.sandip.greedygame.adapter.IconSetsRecyclerAdapter
import com.sandip.greedygame.databinding.ActivityIconDetailsBinding
import com.sandip.greedygame.modal.publicIconSets.Iconset
import com.sandip.greedygame.utils.GreedyGameConstants
import com.sandip.greedygame.viewModel.IconDetailsViewModel

class IconDetailsActivity : AppCompatActivity() {

    private lateinit var activityIconDetailsBinding: ActivityIconDetailsBinding
    private lateinit var iconDetailsViewModel: IconDetailsViewModel
    private lateinit var iconRecyclerAdapter: IconRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityIconDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_icon_details)
        iconDetailsViewModel = ViewModelProvider(this).get(IconDetailsViewModel::class.java)

        val value = intent.extras!!.getInt("iconSetId")

        iconDetailsViewModel.getIconDetails(value)
        iconDetailsViewModel.getAllIcon(value)

        activityIconDetailsBinding.tvReadme.text = resources.getString(R.string.readme)

        initRecyclerView()
        initObservers()
    }

    fun initObservers() {
        iconDetailsViewModel.iconDetailsLiveData.observe(this, Observer {
            if (it.name != null && it.name.length > 0)
                activityIconDetailsBinding.tvIconSetName.text = it.name
            else
                activityIconDetailsBinding.tvIconSetName.text = GreedyGameConstants.NA

            if (it.type != null && it.type.length > 0)
                activityIconDetailsBinding.tvIconSetType.text = it.type
            else
                activityIconDetailsBinding.tvIconSetType.text = GreedyGameConstants.NA

            if (it.prices != null)
                activityIconDetailsBinding.tvPrice.text = it.prices.get(0).currency+" "+it.prices.get(0).price.toString()
            else
                activityIconDetailsBinding.tvPrice.text = GreedyGameConstants.NA

            if (it.author != null && it.author.name.length > 0)
                activityIconDetailsBinding.tvAuthorName.text = it.author.name
            else
                activityIconDetailsBinding.tvAuthorName.text = GreedyGameConstants.NA

            if (it.prices != null && it.prices.get(0).license.name.length > 0)
                activityIconDetailsBinding.tvLicenceName.text = it.prices.get(0).license.name
            else
                activityIconDetailsBinding.tvLicenceName.text = GreedyGameConstants.NA

            if (it.readme != null && it.readme.length > 0)
                activityIconDetailsBinding.tvReadme.text = it.readme
            else
                activityIconDetailsBinding.tvReadme.text = GreedyGameConstants.NA
        })

        iconDetailsViewModel.allIconLiveData.observe(this, Observer {
            if (it!=null && it.icons.size>0){
                iconRecyclerAdapter.setIconList(it.icons)
            }
        })
    }

    fun initRecyclerView() {
        activityIconDetailsBinding.rvIcons.layoutManager = GridLayoutManager(this, 2)
        iconRecyclerAdapter = IconRecyclerAdapter()
        activityIconDetailsBinding.rvIcons.adapter = iconRecyclerAdapter
    }
}