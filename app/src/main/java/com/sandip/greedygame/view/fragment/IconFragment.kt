package com.sandip.greedygame.view.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.sandip.greedygame.R
import com.sandip.greedygame.adapter.IconSetsRecyclerAdapter
import com.sandip.greedygame.adapter.SearchIconsRecyclerAdapter
import com.sandip.greedygame.databinding.FragmentIconBinding
import com.sandip.greedygame.modal.publicIconSets.Iconset
import com.sandip.greedygame.modal.searchIcons.Icon
import com.sandip.greedygame.view.IconDetailsActivity
import com.sandip.greedygame.viewModel.MainActivityViewModel

class IconFragment : Fragment() {

    private lateinit var fragmentIconBinding: FragmentIconBinding
    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var searchIconsRecyclerAdapter: SearchIconsRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentIconBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_icon, container, false)
        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        initSearchIconRecyclerView()

        fragmentIconBinding.ivSearchIcon.setOnClickListener {
            val query = fragmentIconBinding.etSearchIcon.text.toString()
            mainActivityViewModel.getSearchIcons(query)
        }

        mainActivityViewModel.searchIconLiveData.observe(this, Observer {
            if (it!=null && it.size>0){
                searchIconsRecyclerAdapter.setSearchList(it)
            }
        })

        return fragmentIconBinding.root
    }

    fun initSearchIconRecyclerView() {
        fragmentIconBinding.rvIcons.layoutManager = GridLayoutManager(activity,2)
        searchIconsRecyclerAdapter = SearchIconsRecyclerAdapter({selectedItem: Icon -> iconItemClicked(selectedItem)})
        fragmentIconBinding.rvIcons.adapter = searchIconsRecyclerAdapter
    }

    fun iconItemClicked(searchIcon: Icon){
        Toast.makeText(activity, "${searchIcon.icon_id} Selected", Toast.LENGTH_LONG).show()
        val intent = Intent(this.activity, IconDetailsActivity::class.java)
        intent.putExtra("iconSetId", searchIcon.icon_id)
        startActivity(intent)
    }
}