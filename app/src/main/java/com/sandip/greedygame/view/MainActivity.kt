package com.sandip.greedygame.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.sandip.greedygame.R
import com.sandip.greedygame.adapter.ViewPagerAdapter
import com.sandip.greedygame.view.fragment.IconFragment
import com.sandip.greedygame.view.fragment.IconSetFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tabToolbar = findViewById<Toolbar>(R.id.toolbar)
        val tabViewpager = findViewById<ViewPager>(R.id.tab_viewpager)
        val tabTablayout = findViewById<TabLayout>(R.id.tab_tablayout)

        setSupportActionBar(tabToolbar)
        setupViewPager(tabViewpager)

        tabTablayout.setupWithViewPager(tabViewpager)
    }

    private fun setupViewPager(viewpager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        adapter.addFragment(IconSetFragment(), resources.getString(R.string.tab1))
        adapter.addFragment(IconFragment(), resources.getString(R.string.tab2))

        viewpager.adapter = adapter
    }
}