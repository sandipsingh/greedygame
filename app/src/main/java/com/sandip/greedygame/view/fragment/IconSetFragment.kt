package com.sandip.greedygame.view.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.sandip.greedygame.R
import com.sandip.greedygame.adapter.IconSetsRecyclerAdapter
import com.sandip.greedygame.databinding.FragmentIconSetBinding
import com.sandip.greedygame.modal.publicIconSets.Iconset
import com.sandip.greedygame.view.IconDetailsActivity
import com.sandip.greedygame.viewModel.MainActivityViewModel

class IconSetFragment : Fragment() {

    private lateinit var fragmentIconSetBinding: FragmentIconSetBinding
    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var iconSetsRecyclerAdapter: IconSetsRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentIconSetBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_icon_set, container, false)
        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        initIconSetRecyclerView()
        mainActivityViewModel.getAllPublicIconSets()

        mainActivityViewModel.iconSetsLiveData.observe(this, Observer {
            if (it!=null && it.size>0){
                iconSetsRecyclerAdapter.setIconSetList(it)
            }
        })

        return fragmentIconSetBinding.root
    }

    fun initIconSetRecyclerView() {
        fragmentIconSetBinding.rvIconSets.layoutManager = LinearLayoutManager(activity)
        iconSetsRecyclerAdapter = IconSetsRecyclerAdapter({selectedItem: Iconset -> iconSetItemClicked(selectedItem)})
        fragmentIconSetBinding.rvIconSets.adapter = iconSetsRecyclerAdapter
    }

    fun iconSetItemClicked(iconSet: Iconset){
        Toast.makeText(activity, "${iconSet.iconset_id} Selected", Toast.LENGTH_LONG).show()
        val intent = Intent(this.activity, IconDetailsActivity::class.java)
        intent.putExtra("iconSetId", iconSet.iconset_id)
        startActivity(intent)
    }
}